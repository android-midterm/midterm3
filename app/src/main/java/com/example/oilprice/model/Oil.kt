package com.example.oilprice.model

import androidx.annotation.StringRes

data class Oil(
    @StringRes val name:Int,
    @StringRes val price:Int)
